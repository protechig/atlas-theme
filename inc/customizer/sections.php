<?php
/**
 * Customizer sections.
 *
 * @package Atlas Tech 2018
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function ptig_atl_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'ptig_atl_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'atlas-tech' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'ptig_atl_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'atlas-tech' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'atlas-tech' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'ptig_atl_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'atlas-tech' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'ptig_atl_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'atlas-tech' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'ptig_atl_customize_sections' );
