<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Atlas Tech 2018
 */

get_header(); ?>
<div class="grid-x">
<header class="page-header"> 
	  <h1 class="page-title"><span class="title">NEWS<i class="fal fa-newspaper"></i></span></h1>
	  </header><!-- .page-header --> 
	<div class="primary content-area col-l-8">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'single' );

			/*the_post_navigation();*/

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- .primary -->

	<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>
