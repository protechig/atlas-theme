<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Atlas Tech 2018
 */

?>

	</div><!-- #content -->

	<footer id="contact" class="site-footer">
	<div class="grid-x">
		<div class="form-container">
		<header class="footer-header"> 
	<h2 class="heading"><span class="title"><?php the_field( 'contact_header', 'option' ); ?><i class="fal fa-envelope"></i></span></h2> 
	  </header><!-- .page-header --> 
			<p><?php the_field( 'site_loction', 'option' ); ?><br><span style="font-style:italic;"><?php the_field( 'phone_number', 'option' ); ?></span></p>
			<div class="the-form">
				<?php
				$form = get_field( 'gravity_form', 'option' );
				gravity_form( $form, false, true, false, '', true, 1 );
				?>
			</div>

		</div>
		<div class="social-icons">
			 <div class="icons">
				<a href="<?php the_field( 'facebook_url', 'option' ); ?>" target=_blank >
				<span class="fa-stack fa-lg">
					<i class="fab fa-facebook fa-stack-2x fa-inverse"></i>
				</span></a>
				<a href="<?php the_field( 'linkedin_url', 'option' ); ?>" target=_blank><span class="fa-stack fa-lg">
					<i class="fab fa-linkedin fa-stack-2x fa-inverse"></i>
				</span></a>
				<a href="<?php the_field( 'twitter_url', 'option' ); ?>" target=_blank><span class="fa-stack fa-lg">
					<i class="fab fa-twitter fa-stack-2x fa-inverse"></i>
				</span></a>
			</div>
			<div class="line"></div>
		</div>
				

		<div class="subscribe">
			<h2 class="heading"><?php the_field( 'subscribe_form_header', 'option' ); ?></h2>
			<div class="the-form">
				<?php
				$form = get_field( 'email_form', 'option' );
				gravity_form( $form, false, true, false, '', true, 1 );
				?>
			</div>

		</div>	
	</div>	
		<div class="site-info">
			<?php ptig_atl_display_copyright_text(); ?>

		</div><!-- .site-info -->
	</footer><!-- .site-footer container-->
</div><!-- #page -->

<?php wp_footer(); ?>

<nav class="off-canvas-container" aria-hidden="true">
	<button type="button" class="off-canvas-close" aria-label="<?php esc_html_e( 'Close Menu', 'atlas-tech' ); ?>">
		<span class="close"></span>
	</button>
	<?php
		// Mobile menu args.
		$mobile_args = array(
			'theme_location'  => 'mobile',
			'container'       => 'div',
			'container_class' => 'off-canvas-content',
			'container_id'    => '',
			'menu_id'         => 'mobile-menu',
			'menu_class'      => 'mobile-menu',
		);

		// Display the mobile menu.
		wp_nav_menu( $mobile_args );
	?>
</nav>
<div class="off-canvas-screen"></div>
</body>
</html>
