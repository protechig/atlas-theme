<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Atlas Tech 2018
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
