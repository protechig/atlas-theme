<?php
/**
 * Atlas Tech 2018 functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Atlas Tech 2018
 */

if ( ! function_exists( 'ptig_atl_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ptig_atl_setup() {
		/**
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Atlas Tech 2018, use a find and replace
		 * to change 'atlas-tech' to the name of your theme in all the template files.
		 * You will also need to update the Gulpfile with the new text domain
		 * and matching destination POT file.
		 */
		load_theme_textdomain( 'atlas-tech', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'full-width', 1920, 1080, false );
		add_image_size( 'tombstone', 400, 140, false );

		// Register navigation menus.
		register_nav_menus(
			 array(
				 'primary' => esc_html__( 'Primary Menu', 'atlas-tech' ),
				 'mobile'  => esc_html__( 'Mobile Menu', 'atlas-tech' ),
			 )
			);

		/**
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			 'html5', array(
				 'search-form',
				 'comment-form',
				 'comment-list',
				 'gallery',
				 'caption',
			 )
			);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background', apply_filters(
			 'ptig_atl_custom_background_args', array(
				 'default-color' => 'ffffff',
				 'default-image' => '',
			 )
			)
			);

		// Custom logo support.
		add_theme_support(
			 'custom-logo', array(
				 'height'      => 250,
				 'width'       => 500,
				 'flex-height' => true,
				 'flex-width'  => true,
				 'header-text' => array( 'site-title', 'site-description' ),
			 )
			);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif;
add_action( 'after_setup_theme', 'ptig_atl_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ptig_atl_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ptig_atl_content_width', 640 );
}
add_action( 'after_setup_theme', 'ptig_atl_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ptig_atl_widgets_init() {

	// Define sidebars.
	$sidebars = array(
		'sidebar-1' => esc_html__( 'Sidebar 1', 'atlas-tech' ),
		// 'sidebar-2'  => esc_html__( 'Sidebar 2', 'atlas-tech' ),
		// 'sidebar-3'  => esc_html__( 'Sidebar 3', 'atlas-tech' ),
	);

	// Loop through each sidebar and register.
	foreach ( $sidebars as $sidebar_id => $sidebar_name ) {
		register_sidebar(
			 array(
				 'name'          => $sidebar_name,
				 'id'            => $sidebar_id,
				 'description'   => /* translators: the sidebar name */ sprintf( esc_html__( 'Widget area for %s', 'atlas-tech' ), $sidebar_name ),
				 'before_widget' => '<aside class="widget %2$s">',
				 'after_widget'  => '</aside>',
				 'before_title'  => '<h2 class="widget-title">',
				 'after_title'   => '</h2>',
			 )
			);
	}

}
add_action( 'widgets_init', 'ptig_atl_widgets_init' );

/**
 * Enqueue font Awesome
 */
function enqueue_our_required_stylesheets() {
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
}
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Load custom ACF features.
 */
require get_template_directory() . '/inc/acf.php';

/**
 * Load custom filters and hooks.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Load custom queries.
 */
require get_template_directory() . '/inc/queries.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Scaffolding Library.
 */
require get_template_directory() . '/inc/scaffolding.php';
/**
 * Create theme optoins page
 */
if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page(
		array(
			'page_title' => 'Theme General Settings',
			'menu_title' => 'Theme Settings',
			'menu_slug'  => 'theme-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
		);
}

/**
 * Register deals CPT
 */
function deals() {

	$labels = array(
		'name'                  => _x( 'Deals', 'Post Type General Name', 'atlastechgroup' ),
		'singular_name'         => _x( 'Deals', 'Post Type Singular Name', 'atlastechgroup' ),
		'menu_name'             => __( 'Deals', 'atlastechgroup' ),
		'name_admin_bar'        => __( 'Deals', 'atlastechgroup' ),
		'archives'              => __( 'Item Archives', 'atlastechgroup' ),
		'attributes'            => __( 'Item Attributes', 'atlastechgroup' ),
		'parent_item_colon'     => __( 'Parent Item:', 'atlastechgroup' ),
		'all_items'             => __( 'All Items', 'atlastechgroup' ),
		'add_new_item'          => __( 'Add New Item', 'atlastechgroup' ),
		'add_new'               => __( 'Add New', 'atlastechgroup' ),
		'new_item'              => __( 'New Item', 'atlastechgroup' ),
		'edit_item'             => __( 'Edit Item', 'atlastechgroup' ),
		'update_item'           => __( 'Update Item', 'atlastechgroup' ),
		'view_item'             => __( 'View Item', 'atlastechgroup' ),
		'view_items'            => __( 'View Items', 'atlastechgroup' ),
		'search_items'          => __( 'Search Item', 'atlastechgroup' ),
		'not_found'             => __( 'Not found', 'atlastechgroup' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'atlastechgroup' ),
		'featured_image'        => __( 'Featured Image', 'atlastechgroup' ),
		'set_featured_image'    => __( 'Set featured image', 'atlastechgroup' ),
		'remove_featured_image' => __( 'Remove featured image', 'atlastechgroup' ),
		'use_featured_image'    => __( 'Use as featured image', 'atlastechgroup' ),
		'insert_into_item'      => __( 'Insert into item', 'atlastechgroup' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'atlastechgroup' ),
		'items_list'            => __( 'Items list', 'atlastechgroup' ),
		'items_list_navigation' => __( 'Items list navigation', 'atlastechgroup' ),
		'filter_items_list'     => __( 'Filter items list', 'atlastechgroup' ),
	);
	$args   = array(
		'label'               => __( 'Deals', 'atlastechgroup' ),
		'description'         => __( 'Post Type Description', 'atlastechgroup' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'revisions', 'custom-fields', 'page-attributes', 'post-formats' ),
		'taxonomies'          => array( 'deals' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-networking',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'deals', $args );

}
add_action( 'init', 'deals', 0 );
