<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Atlas Tech 2018
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="preload" href="/wp-content/themes/atlas-theme/assets/bower_components/normalize-css/normalize.css" as="style">
	<link rel="preload" href="/wp-content/themes/atlas-theme/assets/bower_components/animate.css/animate.css" as="style">
	<link rel="preload" href="/wp-content/themes/atlas-theme/assets/bower_components/slick/fonts/slick.woff" as="font" type="font/woff2" crossorigin>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'atlas-tech' ); ?></a>

	<header class="site-header header-bg">

		<div class="site-branding">
			<?php the_custom_logo(); ?>
		</div><!-- .site-branding -->
		<!--commented out search button-->

		<button type="button" class="off-canvas-open" aria-expanded="false" aria-label="<?php esc_html_e( 'Open Menu', 'atlas-tech' ); ?>">
			<span class="hamburger"></span>
		</button>

		<nav id="site-navigation" class="main-navigation">
			<?php
				wp_nav_menu(
					array(
						'theme_location' => 'primary',
						'menu_id'        => 'primary-menu',
						'menu_class'     => 'menu dropdown',
					)
					);
			?>

		<div class="social-icons">
					<a href="<?php the_field( 'facebook_url', 'option' ); ?>" target=_blank >
					<span class="fa-stack fa-lg">
						<i class="fab fa-facebook fa-stack-2x fa-inverse"></i>
					</span></a>
					<a href="<?php the_field( 'linkedin_url', 'option' ); ?>" target=_blank><span class="fa-stack fa-lg">
						<i class="fab fa-linkedin fa-stack-2x fa-inverse"></i>
					</span></a>
					<a href="<?php the_field( 'twitter_url', 'option' ); ?>" target=_blank><span class="fa-stack fa-lg">
						<i class="fab fa-twitter fa-stack-2x fa-inverse"></i>
					</span></a>
				</div>
		</nav><!-- #site-navigation -->

	</header><!-- .site-header-->

	<div id="content" class="site-content">
