<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Atlas Tech 2018
 */

?>

<article <?php post_class(); ?>>

<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		if ( 'post' === get_post_type() ) :
		?>
		<div class="entry-meta">
			<?php ptig_atl_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

<?php
if ( has_post_thumbnail() ) {
?>
	<figure class="featured-image index-image">
		<a href="<?php echo esc_url( $article_url ); /*esc_url(get_permalink())*/ ?>" rel="bookmark">
			<?php
the_post_thumbnail( 'blog_grid' );
	?>
		</a>
	</figure><!-- .featured-image full-bleed -->
	<?php } ?>

	<div class="entry-content">

	
		<?php

		the_content(
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			);

			wp_link_pages(
				 array(
					 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'atlas-tech' ),
					 'after'  => '</div>',
				 )
				);

		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php ptig_atl_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
