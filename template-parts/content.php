<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Atlas Tech 2018
 */

?>

<article <?php post_class(); ?>>


<?php
if ( has_post_thumbnail() ) {
?>
	<figure class="featured-image index-image">
		<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
			<?php
the_post_thumbnail( 'blog_grid' );
	?>
		</a>
	</figure><!-- .featured-image full-bleed -->
	<?php } ?>

	<div class="entry-content">

	<header class="entry-header">
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		?>
	</header><!-- .entry-header -->

		<?php

				the_excerpt();
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
