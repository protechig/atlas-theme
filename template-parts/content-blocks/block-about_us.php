<?php
/**
 *  The template used for displaying about us section
 *
 * @package Atlas Tech 2018
 */

// Set up fields.
// Start a <container> with a possible media background.
ptig_atl_display_block_options(
	 array(
		 'container' => 'section', // Any HTML5 container: section, div, etc...
		 'class'     => 'content-block grid-container about-us', // The class of the container.
		 'id'        => 'about-us',
	 )
	);
?> 
  <div id="about" class="grid-x <?php echo esc_attr( $animation_class ); ?>"> 
	<header class="page-header"> 
	<h2 class="page-title"><span><?php the_sub_field( 'header' ); ?><i class="fal fa-compass"></i></span></h2> 
	<p><?php the_sub_field( 'header_description' ); ?></p> 
	  </header><!-- .page-header --> 
  <div class="about-card"> 
  <?php


if ( have_rows( 'about_us_icons' ) ) :
		  while ( have_rows( 'about_us_icons' ) ) :
the_row();
	?>
   
	<div class="cell"> 
		<img src="<?php the_sub_field( 'icons' ); ?>" alt="<?php the_sub_field( 'icons' ); ?>" /> 
		<h3><?php the_sub_field( 'title' ); ?></h3> 
		<p><?php the_sub_field( 'title_information' ); ?></p> 
	</div> 
	<?php
			endwhile;

  endif;

  ?>
   
 </div>
  </div><!-- .grid-x --> 
</section><!-- .fifty-media-text --> 
