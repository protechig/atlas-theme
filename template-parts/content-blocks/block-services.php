<?php
/**
 *  The template used for displaying services section
 *
 * @package Atlas Tech 2018
 */

// Set up fields.
// Start a <container> with a possible media background.
ptig_atl_display_block_options(
	 array(
		 'container' => 'section', // Any HTML5 container: section, div, etc...
		 'class'     => 'content-block grid-container services', // The class of the container.
	 )
	);
?> 
<style> 
	 .services{ 
		background:linear-gradient(  rgba(12, 2, 2, 0.2), rgba(0, 0, 0, 0.2) ), url('<?php the_sub_field( 'background_image' ); ?>'); 
		margin-bottom: 0; 
		background-size: cover !important; 
		background-position: center center; 
		background-attachment: fixed !important; 
	 } 
</style> 
 
  <div id="services" class="grid-x <?php echo esc_attr( $animation_class ); ?>"> 
	<header class="page-header"> 
	<h2 class="page-title"><span><?php the_sub_field( 'header' ); ?><i class="fal fa-pencil-alt"></i></i></span></h2> 
	<p><?php the_sub_field( 'header_intro' ); ?></p> 
	  </header><!-- .page-header --> 
	
 
	<div class="card-board"> 
  <?php
	/**
	 * Check if repeter field has rows of data
	 */
if ( have_rows( 'cards' ) ) :
		  while ( have_rows( 'cards' ) ) :
the_row();
	?>
   
	<div class="cell"> 
		<div class="service-container"> 
			<h3><?php the_sub_field( 'card_title' ); ?></h3> 
			<p><?php the_sub_field( 'card_information' ); ?></p> 
		</div> 
	</div> 
	<?php
			endwhile;

  endif;

  ?>
   
</div> 
  </div><!-- .grid-x --> 
</section><!-- .fifty-media-text -->
