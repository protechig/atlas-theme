<?php
/**
 *  The template used for displaying our team section
 *
 * @package Atlas Tech 2018
 */

// Set up fields.
// Start a <container> with a possible media background.
ptig_atl_display_block_options(
	 array(
		 'container' => 'section', // Any HTML5 container: section, div, etc...
		 'class'     => 'content-block grid-container our-team', // The class of the container.
	 )
	);
?> 
<div id="team" class=" team-content grid-x"> 
   <div class="team-container"> 
   <header class="page-header"> 
	<h2 class="page-title"><span><?php the_sub_field( 'team_header' ); ?></span></h2> 
	<p><?php the_sub_field( 'header_intro' ); ?></p> 
	  </header><!-- .page-header --> 
			<?php
				if ( have_rows( 'member' ) ) :
				while ( have_rows( 'member' ) ) :
				the_row();
				$name = get_sub_field( 'name' );
			?>
	<?php $idname = str_replace( ' ', '-', strtolower( $name ) ); ?>
	<div id="<?php echo esc_html( $idname ); ?>" class="team-container"> 
		<div class="team-wraper"> 
		<div class="team-member"> 
		<i class="fas fa-chevron-down"></i>
			<h3  class="name"> <?php echo esc_html( $name ); ?></h3> 
		</div> 
		<div class="description"> 
			<?php the_sub_field( 'description' ); ?>
			<div class="social"> 
			<a href="<?php the_sub_field( 'linkedin_link' ); ?>" target=_blank><i class="fab fa-linkedin"></i></a> 
			</div> 
			</div> 
		</div> 
	</div> 
		<?php
			endwhile;


endif;

?>
 
</div> 
</section>
