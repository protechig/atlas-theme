<?php
/**
 * Test slider content block
 *
 * @package Atlas Tech 2018
 */

ptig_atl_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container testimonials', // The class of the container.
	)
   );
   ?>
<div id="testimonials">
<?php
$slider_id = get_sub_field( 'slider_to_display' );

if ( function_exists( 'soliloquy' ) ) {
	soliloquy( $slider_id );
}
?>
</div>
</section>
