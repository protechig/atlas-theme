<?php
/**
 * Recent deals template part
 *
 * @package Atlas Tech 2018
 */

$title           = get_sub_field( 'title' );
$post_count      = get_sub_field( 'number_of_posts' );
$categories      = get_sub_field( 'categories' );
$tags            = get_sub_field( 'tags' );
$animation_class = ptig_atl_get_animation_class();

// Variable to hold query args.
$args = array();

// Only if there are either categories or tags.
if ( $categories || $tags ) {
	$args = ptig_atl_get_recent_posts_query_arguments( $categories, $tags );
}

// Always merge in the number of posts.
$args['posts_per_page'] = is_numeric( $post_count ) ? $post_count : 4;

$args['post_type'] = 'deals';

// Get the recent posts.
$recent_posts = ptig_atl_get_recent_posts( $args );

// Display section if we have any posts.
if ( $recent_posts->have_posts() ) :
	// Start a <container> with possible block options.
	wp_enqueue_style( 'slick' );
	wp_enqueue_style( 'slick-theme' );
	wp_enqueue_script( 'slick' );
	wp_enqueue_script( 'deals-slick' );
	ptig_atl_display_block_options(
		array(
			'container' => 'section', // Any HTML5 container: section, div, etc...
			'class'     => 'content-block grid-container recent-posts', // Container class.
		)
	);
	?>

	<div id="deals" class="grid-x">
	<header class="page-header"> 
	  <h1 class="page-title"><span><?php echo esc_attr( $title ); ?></span></h1>
	  </header><!-- .page-header --> 
	</div>
	<div class="recent-deals">
	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">
	<div class="deals-container deals-slick">
			
		<?php
		// Loop through recent posts.
		while ( $recent_posts->have_posts() ) :
			$recent_posts->the_post();
			get_template_part( 'template-parts/content', 'recent-deals' );
		endwhile;
		wp_reset_postdata();
		?>
		</div><!-- .deals-container -->
		<div class="all-deal">
			<a href="/deals">All Deals</a>
		</div>
	
	</div><!-- .grid-x -->

	</div><!-- .deals -->
</section><!-- .recent-posts -->
<?php endif; ?>
