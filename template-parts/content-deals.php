<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Atlas Tech 2018
 */

?> 
<div id="f1_container" class="cells"> 
	<div id="f1_card" class="shadow"> 
		<div class="front face text-align"> 
			<h3 class="title"><?php the_field( 'deal_header' ); ?></h3>
			<div class="deal-info">
				<div class="logo-img"> 
				<?php
					$image = get_field( 'logo' );
					$size  = 'tombstone';
					if ( $image ) {
					?>
					<?php echo wp_get_attachment_image( $image, $size ); ?>
					<?php } ?>
				</div> 
				<span><?php the_field( 'acquired_text' ); ?></span> 
				<div class="bottom-image <?php echo ( get_field( '2_acquisition_logos' ) == '1' ) ? 'two-deals' : 'one-deal'; ?>">
					<div class="logo-img"> 
					<?php
					$image2 = get_field( 'logo_2' );
					$size   = 'tombstone';
					if ( $image2 ) {
					?>
					<?php echo wp_get_attachment_image( $image2, $size ); ?>
					<?php } ?>
					</div> 
					<span><?php the_field( 'acquired_txt' ); ?></span>
					<div class="logo-img"> 
					<?php
					$image = get_field( 'logo_3' );
					$size  = 'tombstone';
					if ( $image ) {
					?>
					<?php echo wp_get_attachment_image( $image, $size ); ?>
					<?php } ?>
					</div> 
				</div>
			</div> 
		   
			<h3 class="amount"><?php the_field( 'amount' ); ?></h3> 

		</div> 
		<div class="back face center"> 
			<a  class="description" href="<?php the_field( 'source_link' ); ?>">
			<?php
			 // Limit to 40 words.
			$value = wp_trim_words( get_field( 'description' ), 40 );
				echo wp_kses_post( $value );
			 ?>
			 </a> 
			
			<a href="<?php the_field( 'source_link' ); ?>" target="_blank" class="button btn">read more</a>
			<span class="footnote"><?php the_field( 'footnote' ); ?></span>
		</div> 
	</div> 
</div>
