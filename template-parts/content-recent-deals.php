<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Atlas Tech 2018
 */

?> 
	<div class="front face text-align"> 
	<a href="<?php the_field( 'source_link' ); ?>" target="_blank">
		<h3 class="title"><?php the_field( 'deal_header' ); ?></h3>
		<div class="deal-info">
			<div class="logo-img"> 
			<?php
					$image = get_field( 'logo' );
					$size  = 'tombstone';
					if ( $image ) {
			?>
			<?php echo wp_get_attachment_image( $image, $size ); ?>
			<?php } ?> 
			</div> 
			<span><?php the_field( 'acquired_text' ); ?></span> 
			<div class="bottom-image <?php echo ( get_field( '2_acquisition_logos' ) == '1' ) ? 'two-deals' : 'one-deal'; ?>">
				<div class="logo-img"> 
				<?php
					$image = get_field( 'logo_2' );
					$size  = 'tombstone';
					if ( $image ) {
					?>
					<?php echo wp_get_attachment_image( $image, $size ); ?>
					<?php } ?> 
				</div> 
				<span><?php the_field( 'acquired_txt' ); ?></span>
				<div class="logo-img"> 
				<?php
					$image = get_field( 'logo_3' );
					$size  = 'tombstone';
					if ( $image ) {
					?>
					<?php echo wp_get_attachment_image( $image, $size ); ?>
					<?php } ?> 
				</div> 
			</div>
		</div> 
		<h3 class="amount"><?php the_field( 'amount' ); ?></h3> 
		</a>
	</div> 

