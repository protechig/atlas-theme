<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package Atlas Tech 2018
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'atlas-tech' ); ?></h2>
</section>
