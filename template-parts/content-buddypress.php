<?php
/**
 * The template used for displaying page content in buddypress.php
 *
 * @package Atlas Tech 2018
 */

?>

<article <?php post_class(); ?>>

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
