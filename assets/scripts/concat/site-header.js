/*eslint space-in-parens: ["error", "never"]*/
/*eslint space-before-function-paren: "error"*/
/*eslint-env es6*/
jQuery(window).on('scroll', function () {
    var yScrollSection = window.pageYOffset;
    var scrollSections = 750;
    if (yScrollSection > scrollSections) {
        jQuery('#headernav').addClass('in-view');
    } else if (250 < yScrollSection) {
        jQuery('#headernav').addClass('scrolled-out');
        jQuery('#headernav').removeClass('in-view');
    } else {
        jQuery('#headernav').removeClass('in-view');

        jQuery('#headernav').removeClass('scrolled-out');
    }
});


jQuery('a[href^="#"]').click(function (e) {
    e.preventDefault();
    var position = jQuery(jQuery(this).attr('href')).offset().top;

    jQuery('body, html').animate({
        scrollTop: position
    } /* speed */);
});
