


jQuery(document).ready(function () {
	var hash = window.top.location.hash;
	if (hash) {

		jQuery(hash + ' .description').slideToggle();
	}
	jQuery('.team-member').click(function () {

		jQuery(this).toggleClass('active');
		jQuery(this)
			.siblings('.description')
			.slideToggle();
	});
});
