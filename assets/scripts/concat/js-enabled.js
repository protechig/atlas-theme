/**
 * File js-enabled.js
 *
 * If Javascript is enabled, replace the <body> class "no-js".
 * eslint space-in-parens: ["error", "never"]
 */
/*eslint space-in-parens: ["error", "never"]*/
document.body.className = document.body.className.replace('no-js', 'js');
