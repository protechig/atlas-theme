<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Atlas Tech 2018
 */

get_header(); ?> 
 
  <div class="primary content-area"> 
	<main id="main" class="site-main"> 
	<header class="page-header"> 
	  <h1 class="page-title"><span class="title">ALL DEALS<i class="far fa-briefcase"></i></span></h1>
	  </header><!-- .page-header --> 
	<?php

		$args               = array(
			'post_type'      => 'deals',
			'year'           => date( 'Y' ),
			'posts_per_page' => -1,
		);
		$current_year_query = new WP_Query( $args );
	?>
 
	<?php if ( $current_year_query->have_posts() ) : ?> 
 
	  <header class="page-header text-align"> 
	  <h1 class="page-heading"><span class="date"><?php echo get_the_date( 'Y' ); ?></span></h1>
	  </header><!-- .page-header --> 
<div class="deals"> 
	   <div class="grid-x">   
	<div class="deals-container"> 
 
	  <?php
	  /* Start the Loop */
	  while ( $current_year_query->have_posts() ) :
			$current_year_query->the_post();

			/*
			* Include the Post-Format-specific template for the content.
			* If you want to override this in a child theme, then include a file
			* called content-___.php (where ___ is the Post Format name) and that will be used instead.
			*/
			get_template_part( 'template-parts/content', 'deals' );

	  endwhile;

	endif;
	wp_reset_postdata();
?>
</div> 
  </div> 
  </div> 
	<?php
	$args['year']         = '';
	$args['date_query']   = array(
		array(
			'before'    => array(
				'year'  => date( 'Y' ),
				'month' => 1,
				'day'   => 1,
			),
			'inclusive' => false,
		),
	);
	$previous_deals_query = new WP_Query( $args );
	if ( $previous_deals_query->have_posts() ) :

	?>
	<header class="page-header"> 
	  <h1 class="page-heading"><span>PAST DEALS</span></h1>
	  </header><!-- .page-header --> 
<div class="deals"> 
	   <div class="grid-x">   
	<div class="deals-container"> 
	<?php
		while ( $previous_deals_query->have_posts() ) :
$previous_deals_query->the_post();

			/*
			* Include the Post-Format-specific template for the content.
			* If you want to override this in a child theme, then include a file
			* called content-___.php (where ___ is the Post Format name) and that will be used instead.
			*/
			get_template_part( 'template-parts/content', 'deals' );

	  endwhile;

	else :

	  get_template_part( 'template-parts/content', 'none' );

	endif;
wp_reset_postdata();
	?>
	 
</div> 
  </div> 
	</div> 

	</main><!-- #main --> 
  </div><!-- .primary --> 
<?php get_footer(); ?>
